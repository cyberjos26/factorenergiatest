<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 15/4/2021
 * Time: 5:33 PM
 */

namespace App\Http\Funtionalities;


use Config;
use DateTime;
use GuzzleHttp\Client;

class ForoFun
{

    public function ConvertDataTimeUnix($date)
    {
        $date = new DateTime($date);
        $dateUnix = $date->format('U');
        return $dateUnix;
    }

    public function CreateLinkToRequest($tagged, $todate, $fromdate)
    {

        $newUrl = "";
        $urlBase = 'questions?order=desc&sort=activity&site=stackoverflow';


            if($todate!==null && $fromdate==null)
            {
                $newUrl = $urlBase.'&tagged='.$tagged.'&todate='.$todate;
            }
            else
                if($todate==null && $fromdate!==null)
                {
                    $newUrl = $urlBase.'&tagged='.$tagged.'&fromdate='.$fromdate;
                }
                else
                    if($todate!==null && $fromdate!==null)
                    {
                        $newUrl = $urlBase.'&tagged='.$tagged.'&todate='.$todate.'&fromdate='.$fromdate;
                    }
                    else
                    {
                        $newUrl = $urlBase.'&tagged='.$tagged;
                    }

        return $newUrl;

    }

    public function CreateAPIRequest($url)
    {
        $foro = new Client([
            'base_uri' => Config::get('app.API_URL'),
            'timeout'  => Config::get('app.TIME_OUT'),
        ]);

        $response = $foro->request('GET', $url, ['verify' => false]);

        return json_decode($response->getBody()->getContents(),true);
    }
}