<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 15/4/2021
 * Time: 6:04 PM
 */

namespace App\Http\Validations;


use Validator;

class ForoValidations
{
    public function ValidateForoDataRequest($request)
    {
        $tagged = $request->tagged;
        $todate = $request->todate;
        $fromdate = $request->fromdate;

        $data =
            [
                'tagged' => $tagged,
                'todate' => $todate,
                'fromdate' => $fromdate
            ];

        if($todate!==null && $fromdate==null)
        {
            $rules =
                [
                    'tagged' => 'required',
                    'todate' => 'date_format:Y-m-d'
                ];
        }
        else
            if($fromdate!==null && $todate==null)
            {
                $rules =
                    [
                        'tagged' => 'required',
                        'fromdate' => 'date_format:Y-m-d'
                    ];
            }
            else
                if($fromdate!==null && $todate!==null)
                {
                    $rules =
                        [
                            'tagged' => 'required',
                            'todate' => 'date_format:Y-m-d',
                            'fromdate' => 'date_format:Y-m-d'
                        ];
                }
            else
            {
                $rules =
                    [
                        'tagged' => 'required'
                    ];
            }

        $message =
            [
              'tagged.required' => 'Is neccesary a tagget for make a filter ex: php, jquery, ajax etc..',
              'todate.date_format' => 'The todate format must be YYYY-MM-DD',
              'fromdate.date_format' => 'The fromdate format must be YYYY-MM-DD',
            ];

        $v = Validator::make($data,$rules,$message);

        if($v->fails())
        {
            return $v->errors();
        }

    }
}