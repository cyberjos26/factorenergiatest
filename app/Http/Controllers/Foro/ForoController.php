<?php

namespace App\Http\Controllers\Foro;

use App\Http\Controllers\Controller;
use App\Http\Funtionalities\ForoFun;
use App\Http\Validations\ForoValidations;
use Illuminate\Http\Request;

class ForoController extends Controller
{
    public function GetDataForoQuestions(Request $request)
    {
        $tagged = $request->tagged;
        $todate = $request->todate;
        $fromdate = $request->fromdate;

        $foroValidations = new ForoValidations();
        $functionalities = new ForoFun();

        /*In this point will validate all element sending at the link*/
        $errors = $foroValidations->ValidateForoDataRequest($request);

        if($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
        {
            /*In this point create datatime in Unix format because is neccesary for the API*/
            if($todate!==null)
            {
                $todate = $functionalities->ConvertDataTimeUnix($request->todate);
            }
            if($fromdate!==null)
            {
                $fromdate = $functionalities->ConvertDataTimeUnix($request->fromdate);
            }

            /*Create new link with parameters for search questions in the foro*/
            $newLink = $functionalities->CreateLinkToRequest($tagged,$todate,$fromdate);

            /*Create API Request*/
            $apiRequest=$functionalities->CreateAPIRequest($newLink);

            return response()->json
            ([
               'status' =>'ok',
                $apiRequest
            ],200);


        }
    }
}
