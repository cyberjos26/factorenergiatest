## This is a site use the Laravel Framework 5.8 how API engine
##### Is important for beging you need:
    - php: 7.2.* (Minimum)
    - composer: 2.* (recommend)    

##### After to copy repository please execute:
      \>: composer update
    
## Link Site
The URL for use this site is **http://127.0.0.1/factorenergiatest/public/**

## Api functionality

- **[Questions with all data | GET](#)| foro/questionsall/{tagged}/{todate?}/{fromdate?}**
##### Obtain to stackoverflow foro all questions with tagged and optionaly you can search the question with to date and from date fields, IMPORTANT todate and fromdate are optional fields. If you want search only with tagged, you can do it using this link. For make this search is neccesary this attributes:
    - tagged    => tagget for search questions in the foro: php, laravel, ajax, html etc...
    - todate    => Search in the foro all question to date inidicate.
    - fromdate  => Search in the foro all question from date inidicate.  
.
.
.
For create a get the estructure must be like it:

    "tagged":int,varchar
    "todate": date(YYYY-MM-DD),
    "fromdate": date(YYYY-MM-DD)

If exist a error in validation this return 422 code:

    "errors": {
        "attribute": [
            "message"
        ]
    }

If result is ok this return a JSON format with the result and 200 code:

    "json result",
    "status": ok

- **[Questions with to date | GET](#)| foro/questionswithtodate/{tagged}/{todate?}**
##### Obtain to stackoverflow foro all questions with tagget and optionaly you can search the question with to date field, IMPORTANT todate is optional field. If you want search only with tagged you can do it using this link. For make this search is neccesary this attributes:
    - tagged    => tagget for search questions in the foro: php, laravel, ajax, html etc...
    - todate    => Search in the foro all question to date inidicate.     
.
.
.
For create a get the estructure must be like it:

    "tagged": int,varchar
    "todate": date(YYYY-MM-DD),    

If exist a error in validation this return 422 code:

    "errors": {
        "attribute": [
            "message"
        ]
    }

If result is ok this return a JSON format with the result and 200 code:

    "json result",
    "status": ok
    
- **[Questions with from date | GET](#)| foro/questionswithfromdate/{tagged}/{fromdate?}**
##### Obtain to stackoverflow foro all questions with tagget and optionaly you can search the question with from date field, IMPORTANT fromdate is optional field. If you want search only with tagged you can do it using this link. For make this search is neccesary this attributes:
    - tagged    => tagget for search questions in the foro: php, laravel, ajax, html etc...
    - fromdate    => Search in the foro all question to from inidicate.     
.
.
.
For create a get the estructure must be like it:

    "tagged": int,varchar
    "fromdate": date(YYYY-MM-DD),    

If exist a error in validation this return 422 code:

    "errors": {
        "attribute": [
            "message"
        ]
    }

If result is ok this return a JSON format with the result and 200 code:

    "json result",
    "status": ok