<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('foro/questionsall/{tagged}/{todate?}/{fromdate?}',['as'=>'foro','uses'=>'Foro\ForoController@GetDataForoQuestions']);
Route::get('foro/questionswithtodate/{tagged}/{todate?}',['as'=>'foro','uses'=>'Foro\ForoController@GetDataForoQuestions']);
Route::get('foro/questionswithfromdate/{tagged}/{fromdate?}',['as'=>'foro','uses'=>'Foro\ForoController@GetDataForoQuestions']);
